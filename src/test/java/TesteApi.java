import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.junit.Test;
import javax.swing.*;
import static io.restassured.RestAssured.given;


public class TesteApi {

    String id;
    @Test
    public void SimulacaoCriandoNovoRegistro(){
        String url  = "http://localhost:8080/api/v1/simulacoes";

        String corpo = "{\n" +
                "  \"nome\": \"thais souza\",\n" +
                "  \"cpf\": 11193236015,\n" +
                "  \"email\": \"thais@email.com\",\n" +
                "  \"valor\": 1200,\n" +
                "  \"parcelas\": 3,\n" +
                "  \"seguro\": true\n" +
                "}";

        //Dado que eu tenha
            Response response = given().contentType("application/json").body(corpo).
                when().post(url);
            response.then().statusCode(201);
                    System.out.println("Retorno =>" + response.body().asString());

    }

    @Test
    public void SimulacaoInformandoCpfDuplicado(){
        String url  = "http://localhost:8080/api/v1/simulacoes/44433344421";

        String corpo = "{\n" +
                "  \"nome\": \"thais de Tal\",\n" +
                "  \"cpf\": 44433344421,\n" +
                "  \"email\": \"thais@email.com\",\n" +
                "  \"valor\": 1200,\n" +
                "  \"parcelas\": 3,\n" +
                "  \"seguro\": true\n" +
                "}";

        //Dado que eu tenha
            Response response = given().contentType("application/json").body(corpo).
                when().post(url);
                response.then().statusCode(409);
                System.out.println("Retorno =>" + response.body().asString());

    }

    @Test
    public void SimulacaoComErros(){
        String url  = "http://localhost:8080/api/v1/simulacoes";

        String corpo = "{\n" +
                "  \"nome\": \"thais de Tal\",\n" +
                "  \"cpf\": 99933344421,\n" +
                "  \"email\": \"thais\",\n" +
                "  \"valor\": 1200,\n" +
                "  \"parcelas\": 3,\n" +
                "  \"seguro\": true\n" +
                "}";

        //Dado que eu tenha
        Response response = given().contentType("application/json").body(corpo).
                when().post(url);
        response.then().statusCode(400);
        System.out.println("Retorno =>" + response.body().asString());

    }
    @Test
    public void ConsultarTodasSimulacoes(){
        String url  = "http://localhost:8080/api/v1/simulacoes";

        String corpo = "{\n" +
                "  \"nome\": \" \",\n" +
                "  \"cpf\": ,\n" +
                "  \"email\": \"\",\n" +
                "  \"valor\": ,\n" +
                "  \"parcelas\":,\n" +
                "  \"seguro\":\n" +
                "}";

        //Dado que eu tenha
        Response response = given().contentType("application/json").body(corpo).
                when().get(url);
        response.then().statusCode(200);
        System.out.println("Retorno =>" + response.body().asString());

    }

    @Test
    public void AlterandoSimulação(){
        String url  = "http://localhost:8080/api/v1/simulacoes/99933344421";

        String corpo = "{\n" +
                "  \"nome\": \"thais de Tal\",\n" +
                "  \"cpf\": 99933344421,\n" +
                "  \"email\": \"fulano@gmail.com\",\n" +
                "  \"valor\": 1200,\n" +
                "  \"parcelas\": 3,\n" +
                "  \"seguro\": true\n" +
                "}";

        //Dado que eu tenha
        Response response = given().contentType("application/json").body(corpo).
                when().put(url);
        response.then().statusCode(200);
        System.out.println("Retorno =>" + response.body().asString());

    }

    @Test
    public void ConsultandoPorCpfComErro404(){
        String url  = "http://localhost:8080/api/v1/simulacoes/00000000000";

        String corpo = "{\n" +
                "  \"nome\": \"fulano de Tal\",\n" +
                "  \"cpf\": 00000000000,\n" +
                "  \"email\": \"fulano@gmail.com\",\n" +
                "  \"valor\": 1200,\n" +
                "  \"parcelas\": 3,\n" +
                "  \"seguro\": true\n" +
                "}";

        //Dado que eu tenha
        Response response = given().contentType("application/json").body(corpo).
                when().get(url);
        response.then().statusCode(404);
        System.out.println("Retorno =>" + response.body().asString());

    }

    @Test
    public void ConsultandoPorCpf(){
        String url  = "http://localhost:8080/api/v1/simulacoes/97093236014";

        String corpo = "{\n" +
                "  \"nome\": \"fulano de Tal\",\n" +
                "  \"cpf\": 97093236014,\n" +
                "  \"email\": \"fulano@gmail.com\",\n" +
                "  \"valor\": 1200,\n" +
                "  \"parcelas\": 3,\n" +
                "  \"seguro\": true\n" +
                "}";

        //Dado que eu tenha
        Response response = given().contentType("application/json").body(corpo).
                when().get(url);
        response.then().statusCode(200);
        System.out.println("Retorno =>" + response.body().asString());

    }

@Test
    public void DetetandoSimulacao(){
        SimulacaoCriandoNovoRegistro();
        DetetaSimulacao();

    }
    @Test
    public void DetetaSimulacao(){
        String url  = "http://localhost:8080/api/v1/simulacoes/{id}";

        //Dado que eu tenha
        Response response =
                given()
                .contentType("application/json")
                        .pathParam("id", id)
                .when().delete(url);
        response.then().statusCode(200);
        System.out.println("Retorno =>" + response.body().asString());


    }
}
